//
//  SelectionDelegate.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/11/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

protocol CategorySelectionDelegate: class {
    func categorySelected(_ newCategory: Category)
}

protocol PostSelectionDelegate: class {
    func postSelected(_ newPost: Post)
}
