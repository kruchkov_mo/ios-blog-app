//
//  MainViewController.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/7/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    weak var categoryDelegate: CategorySelectionDelegate?

    @IBOutlet weak var authChildView: UIView!
    @IBOutlet weak var categoryTableView: UITableView!
    
    @IBOutlet weak var loadingStatus: UIActivityIndicatorView!
    
    lazy var viewModel: CategoryViewModel = {
        return CategoryViewModel()
    }()
    
    func initViewModel() {
        viewModel.updateCategoriesClojure = { [weak self] () in
            DispatchQueue.main.async {
                self?.categoryTableView.reloadData()
            }
        }
        
        viewModel.updateCatLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isCatLoading ?? false
                if isLoading {
                    self?.loadingStatus.startAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.categoryTableView.alpha = 0.0
                    })
                } else {
                    self?.loadingStatus.stopAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.categoryTableView.alpha = 1.0
                    })
                }
            }
        }
        
        viewModel.afterDataLoadingClojure = { [weak self] () in
            DispatchQueue.main.async {
                if let firstCategory = self?.viewModel.getFirstCategory() {
                    self?.categoryDelegate?.categorySelected(firstCategory)
                }
            }
        }
        
        viewModel.initFetch()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTableView.tableFooterView = UIView()
        
        // init view model
        initViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onTapSettings(_ sender: UIBarButtonItem) {
        guard let settingsViewController = storyboard?.instantiateViewController(withIdentifier: "SettingsTableViewController") else {
            return
        }
        
        settingsViewController.modalPresentationStyle = .popover
        settingsViewController.modalTransitionStyle = .coverVertical
        settingsViewController.popoverPresentationController?.delegate = self
        settingsViewController.popoverPresentationController?.barButtonItem = sender
        
        present(settingsViewController, animated: true)
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    */
}

extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCategories
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "category_cell", for: indexPath)
        
        guard let categoryCell = cell as? CategoryTableViewCell else {
            fatalError("Category cell doesn\'t exists")
        }
        let cellVM = viewModel.getCategoryCell(at: indexPath)
        categoryCell.catTitle.text = cellVM.name
        categoryCell.catDescription.text = cellVM.description
        categoryCell.accessoryType = .disclosureIndicator
        return categoryCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCategory = viewModel.getCategoryCell(at: indexPath)
        categoryDelegate?.categorySelected(selectedCategory)
        
        if let detailViewController = categoryDelegate as? PostListViewController,
            let detailNavigationController = detailViewController.navigationController {
            splitViewController?.showDetailViewController(detailNavigationController, sender: nil)
        }
    }
}

// MARK: - UIPopoverPresentationControllerDelegate
extension MainViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.overCurrentContext
    }
    
    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}
