//
//  CategoryViewModel.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/7/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CategoryViewModel {
    
    static let API_URL = ResourceUtil.getApiUrl(key: "categories_api")
    
    var selectedCategory: Category?
    var updateCategoriesClojure: (()->())?
    var updateCatLoadingStatus: (()->())?
    var afterDataLoadingClojure: (()->())?
    
    private var categoryArray: [Category] = [Category]() {
        didSet {
            self.updateCategoriesClojure?()
        }
    }
    
    var numberOfCategories: Int {
        return categoryArray.count
    }
    
    func getCategoryCell(at indexPath: IndexPath) -> Category {
        return categoryArray[indexPath.row]
    }
    
    var isCatLoading: Bool = false {
        didSet {
            self.updateCatLoadingStatus?()
        }
    }
    
    func initFetch() {
        self.getCategories()
    }
    
    func getFirstCategory() -> Category? {
        return self.categoryArray.first
    }
    
    func getCategories() {
        self.isCatLoading = true
        Alamofire.request(CategoryViewModel.API_URL)
            .responseJSON { response in
                if response.result.isSuccess {
                    self.isCatLoading = false
                    if let d = response.data, let json = try? JSON(data: d) {
                        for (_, item) in json {
                            let c = Category(categoryId: Int(item["id"].stringValue) ?? 0,
                                             description: item["description"].stringValue,
                                             name: item["name"].stringValue)
                            self.categoryArray.append(c)
                        }
                    }
                    self.afterDataLoadingClojure?()
                } else {
                    DispatchQueue.global(qos: .background).async {
                        print("Network request fails, retry after timeout: \(NetworkUtil.FETCHING_TIMEOUT) ms")
                        usleep(useconds_t(NetworkUtil.FETCHING_TIMEOUT * 1000))
                        self.getCategories()
                    }
                }
        }
    }
}

extension CategoryViewModel {
    func userPressed( at indexPath: IndexPath ) {
        selectedCategory = self.categoryArray[indexPath.row]
    }
}
