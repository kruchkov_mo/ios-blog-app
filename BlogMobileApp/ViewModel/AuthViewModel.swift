//
//  AuthViewModel.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/17/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

import Firebase

class AuthViewModel {
    
    var updateAuthUI: (()->())?
    
    func signOut() -> Bool {
        do {
            try getAuth().signOut()
        } catch let signOutError as NSError {
            print("Error signing out: \(signOutError)")
            return false
        }
        updateAuthUI?()
        return true
    }
    
    func getAuth() -> Auth {
        return Auth.auth()
    }
}
