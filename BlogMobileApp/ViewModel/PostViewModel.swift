//
//  PostViewModel.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/7/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class PostViewModel {
    static let API_URL = ResourceUtil.getApiUrl(key: "posts_api")
    
    var selectedPost: Post?
    var updatePostsClojure: (()->())?
    var updateLoadingStatus: (()->())?
    
    private var postArray: [Post] = [Post]() {
        didSet {
            self.updatePostsClojure?()
        }
    }
    
    var numberOfPosts: Int {
        return postArray.count
    }
    
    func getPostCell(at indexPath: IndexPath) -> Post {
        return postArray[indexPath.row]
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    func getFirstPost() -> Post? {
        return self.postArray.first
    }
    
    func getPosts(categoryId id: Int) {
        self.isLoading = true
        let url = String(format: PostViewModel.API_URL, id)
        Alamofire.request(url)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    self.isLoading = false
                    if let d = response.data, let json = try? JSON(data: d) {
                        var posts: [Post] = []
                        for (_, item) in json {
                            let p = Post(postId: Int(item["id"].stringValue) ?? 0,
                                         userId: Int(item["author"].stringValue) ?? 0,
                                         title: item["title"].stringValue,
                                         body: item["content"].stringValue)
                            posts.append(p)
                        }
                        self.postArray = posts
                    }
                } else {
                    DispatchQueue.global(qos: .background).async {
                        print("Network request fails, retry after timeout: \(NetworkUtil.FETCHING_TIMEOUT) ms")
                        usleep(useconds_t(NetworkUtil.FETCHING_TIMEOUT * 1000))
                        self.getPosts(categoryId: id)
                    }
                }
        }
    }
}

extension PostViewModel {
    func userPressed(at indexPath: IndexPath ) {
        selectedPost = self.postArray[indexPath.row]
    }
}

