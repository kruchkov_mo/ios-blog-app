//
//  PostListViewController.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/7/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

class PostListViewController: UIViewController {

    var currentPost: Post?
    
    weak var categoryDelegate: CategorySelectionDelegate?
    
    var category: Category? {
        didSet {
            updatePostList()
        }
    }
    
    @IBOutlet weak var postLoadingStatus: UIActivityIndicatorView!
    @IBOutlet weak var postTableView: UITableView!
    
    lazy var viewModel: PostViewModel = {
        return PostViewModel()
    }()
    
    func initViewModel() {
        viewModel.updatePostsClojure = { [weak self] () in
            DispatchQueue.main.async {
                self?.postTableView.reloadData()
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                if isLoading {
                    self?.postLoadingStatus.startAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.postTableView.alpha = 0.0
                    })
                } else {
                    self?.postLoadingStatus.stopAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.postTableView.alpha = 1.0
                    })
                }
            }
        }
    }
    
    func updatePostList() {
        // guarantee that the view is loaded
        loadViewIfNeeded()
        if let c = category {
            self.navigationItem.title = c.name
            self.viewModel.getPosts(categoryId: c.categoryId)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postTableView.tableFooterView = UIView()
        
        self.initViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let postVC = segue.destination as? PostViewController {
            postVC.post = currentPost
        }
    }
}

extension PostListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfPosts
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "post_cell", for: indexPath)
        
        guard let postCell = cell as? PostTableViewCell else {
            fatalError("Post cell doesn\'t exists")
        }
        let cellVM = viewModel.getPostCell(at: indexPath)
        postCell.postTitle?.text = cellVM.title
        return postCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentPost = viewModel.getPostCell(at: indexPath)
        performSegue(withIdentifier: "segue_view_post", sender: self)
    }
}

extension PostListViewController: CategorySelectionDelegate {
    func categorySelected(_ newCategory: Category) {
        category = newCategory
    }
}
