//
//  SettingsTableViewController.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/12/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var themeSelector: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        themeSelector.selectedSegmentIndex = BlogTheme.current.rawValue
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissAnimated))
        
    }
}
    
    // MARK: - Actions
private extension SettingsTableViewController {
        
    @objc func dismissAnimated() {
        dismiss(animated: true)
    }
    
    @IBAction func applySettings(_ sender: UIButton) {
        if let selectedTheme = BlogTheme(rawValue: themeSelector.selectedSegmentIndex) {
            selectedTheme.apply()
        }
        dismissAnimated()
    }
}
