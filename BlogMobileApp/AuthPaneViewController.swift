//
//  AuthPaneViewController.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/14/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

import SDWebImage

import Firebase
import GoogleSignIn

class AuthPaneViewController: UIViewController {
    
    var ref: DatabaseReference!
    
    lazy var viewModel: AuthViewModel = {
        return AuthViewModel()
    }()
    
    @IBAction func signOutTap(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        if !viewModel.signOut() {
            self.authAlert(title: "Authentification error", message: "Google sign out fails")
        }
    }
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    
    @IBAction func authButtonTap(_ sender: UIButton) {
        
        guard let authViewController = storyboard?.instantiateViewController(withIdentifier: "AuthViewController") as? AuthViewController else {
            return
        }
        
        authViewController.modalPresentationStyle = .popover
        authViewController.modalTransitionStyle = .coverVertical
        authViewController.popoverPresentationController?.delegate = self
        authViewController.popoverPresentationController?.sourceView = sender
        
        present(authViewController, animated: true)
    }
    
    func initViewModel() {
        viewModel.updateAuthUI = { [weak self] () in
            DispatchQueue.main.async {
                if let user = Auth.auth().currentUser {
                    self?.userName.text = user.displayName
                    self?.userEmail.text = user.email
                    self?.userAvatar.sd_setImage(with: user.photoURL)
                    self?.signInButton.isHidden = true
                    self?.signOutButton.isHidden = false
                } else {
                    self?.userName.text = ""
                    self?.userEmail.text = "Anonymouse"
                    self?.userAvatar.image = UIImage(named: "anon_ava.png")
                    self?.signOutButton.isHidden = true
                    self?.signInButton.isHidden = false
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // For Google auth
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        initViewModel()
        
        self.ref = Database.database().reference()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.updateAuthUI?()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    fileprivate func authAlert(title: String, message msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

    // MARK: - UIPopoverPresentationControllerDelegate
extension AuthPaneViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.overCurrentContext
    }
    
    func presentationController(_ controller: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        return UINavigationController(rootViewController: controller.presentedViewController)
    }
}

    // MARK: - GIDSignInDelegate for Google auth
extension AuthPaneViewController: GIDSignInDelegate {
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let e = error {
            print(e.localizedDescription)
            return
        }
        guard let authentication = user.authentication else { return }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
            
            if let e = error {
                self.authAlert(title: "Authentification error", message: "Google authentification fails")
                print(e.localizedDescription)
            } else {
                self.viewModel.updateAuthUI?()
                if let user = Auth.auth().currentUser {
                    self.ref.child("users").child(user.uid).setValue(["user_name": user.displayName, "user_email": user.email])
                }
            }
        }
    }
}
