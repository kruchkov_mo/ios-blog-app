//
//  PostViewController.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/11/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit
import WebKit

import Firebase

class PostViewController: UIViewController {

    var post: Post?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyView: WKWebView!
    @IBOutlet weak var postLikes: UILabel!
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBAction func likeIt(_ sender: UIButton) {
        if let p = post, let u = Auth.auth().currentUser {
            let postLikesRef = self.ref.child("likes").child(String(p.postId))
            postLikesRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let userList: [String] = snapshot.value as? [String] ?? []
                if !userList.contains(u.uid) {
                    postLikesRef.child(String(userList.count)).setValue(u.uid)
                    self.likeButton.isEnabled = false
                }
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.likeButton.isEnabled = false
        self.postLikes.text = ""
        
        if let p = post {
            titleLabel.text = p.title
            let postHtml = p.body.replacingOccurrences(of: "http://", with: "https://", options: .literal, range: nil)
            bodyView.loadHTMLString(postHtml, baseURL: nil)
            
            // fb
            self.ref = Database.database().reference()
            
            self.ref.child("posts").child(String(p.postId)).setValue(["post_id": p.postId])
            self.ref.child("likes").child(String(p.postId)).observe(DataEventType.value, with: { (snapshot) in
                let likeArray: [String] = snapshot.value as? [String] ?? []
                
                if let u = Auth.auth().currentUser, !likeArray.contains(u.uid) {
                    self.likeButton.isEnabled = true
                }
                if likeArray.count > 0 {
                    self.postLikes.text = "\(likeArray.count) people liked this post"
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
