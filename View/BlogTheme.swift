//
//  Theme.swift
//  NavBarTest
//
//  Created by max kruchkov on 9/18/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

enum BlogTheme: Int {
    case `default`, dark
    
    private static let selectedTheme = "SelectedTheme"
    
    static var current: BlogTheme {
        let storedTheme = UserDefaults.standard.integer(forKey: BlogTheme.selectedTheme)
        return BlogTheme(rawValue: storedTheme) ?? .default
    }
    
    var statusBarStyle: UIStatusBarStyle {
        switch self {
            case .default:
                return UIStatusBarStyle.default
            case .dark:
                return UIStatusBarStyle.lightContent
        }
    }
    
    var navBarStyle: UIBarStyle {
        switch self {
            case .default:
                return UIBarStyle.default
            case .dark:
                return UIBarStyle.black
        }
    }
    
    var tintColor: UIColor? {
        switch self {
            case .default:
                return nil
            case .dark:
                return UIColor.white
        }
    }
    
    var tintBgColor: UIColor? {
        switch self {
        case .default:
            return nil
        case .dark:
            return UIColor(red: 77/255, green: 76/255, blue: 101/255, alpha: 1)
        }
    }
    
    var tableBgColor: UIColor? {
        switch self {
        case .default:
            return UIColor.white
        case .dark:
            return UIColor(red: 77/255, green: 76/255, blue: 101/255, alpha: 1)
        }
    }
    
    var backBgColor: UIColor {
        switch self {
        case .default:
            return UIColor.white
        case .dark:
            return UIColor(red: 77/255, green: 76/255, blue: 101/255, alpha: 1)
        }
    }
    
    var labelFont: UIFont? {
        if self == .dark {
            return UIFont(name: "Avenir Heavy", size: 20.0)
        }
        return nil
    }
    
    var selectedCellColor: UIColor? {
        switch self {
            case .default:
                return nil
            case .dark:
                return UIColor(red: 137/255, green: 136/255, blue: 139/255, alpha: 1)
        }
    }
    
    var buttonBgColor: UIColor? {
        switch self {
        case .default:
            return nil
        case .dark:
            return UIColor.clear
        }
    }
    
    var tableCellBgColor: UIColor? {
        switch self {
        case .default:
            return nil
        case .dark:
            return UIColor(red: 207/255, green: 216/255, blue: 220/255, alpha: 1)
        }
    }
    
    var tableViewSeparatorColor: UIColor? {
        switch self {
        case .default:
            return nil
        case .dark:
            return UIColor(red: 22/255, green: 24/255, blue: 25/255, alpha: 1)
        }
    }
    
    func apply() {
        // Save to storage
        UserDefaults.standard.set(rawValue, forKey: BlogTheme.selectedTheme)
        UserDefaults.standard.synchronize()
        
        // Common
        UIApplication.shared.delegate?.window??.tintColor = tintColor
        UINavigationBar.appearance().barStyle = navBarStyle
        UIApplication.shared.statusBarStyle = statusBarStyle
        
        // Font
        if let font = labelFont {
            UILabel.appearance().font = font
        }
        
        // Label
        UILabel.appearance().textColor = tintColor
        UILabel.appearance().backgroundColor = buttonBgColor
        
        // Table
        UITableView.appearance().backgroundColor = tableBgColor
        UITableView.appearance().separatorColor = tableViewSeparatorColor
        
        // Table cell
        UITableViewCell.appearance().accessoryType = .disclosureIndicator
        UITableViewCell.appearance().layer.borderColor = tintBgColor?.cgColor
        let bgColorView = UIView()
        bgColorView.backgroundColor = selectedCellColor
        UITableViewCell.appearance().selectedBackgroundView = bgColorView
        //UITableViewCell.appearance().textLabel?.textColor = UIColor.blue   //UIColor(157, 166, 169)
        UITableViewCell.appearance().backgroundColor = tableCellBgColor
        UILabel.appearance(whenContainedInInstancesOf: [UITableView.self]).textColor = tintBgColor
        
        // Button
        UIButton.appearance().backgroundColor = buttonBgColor
        UIButton.appearance().tintColor = tintColor
        
        UIAuthButton.appearance().borderWidth = 1
        UIAuthButton.appearance().borderColor = tintColor
        UIAuthButton.appearance().titleEdgeInsets = UIEdgeInsets(top: 1.0, left: 5.0, bottom: 1.0, right: 5.0)
        
        // Other
        UIBackView.appearance().backgroundColor = backBgColor
        
        // Navigation Bar
        let navBarColors: [UIColor]
        if self == .dark {
            UIGradientNavigationBar.appearance().titleTextAttributes = [.foregroundColor: tintColor!]
            navBarColors = [
                UIColor(157, 166, 169),
                UIColor(120, 139, 145)
            ]
        } else {
            UIGradientNavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(0, 122, 255)]
            navBarColors = [UIColor.white, UIColor.white]
        }
        
        UIGradientNavigationBar.appearance().colors = navBarColors
        UIGradientNavigationBar.appearance().startPoint = CGPoint(x: 0, y: 0)
        UIGradientNavigationBar.appearance().endPoint = CGPoint(x: 1, y: 0)
    }
}

extension UIColor {
    convenience init(_ r: Int, _ g: Int, _ b: Int) {
        self.init(red: CGFloat(r)/255, green: CGFloat(g)/255, blue: CGFloat(b)/255, alpha: 1)
    }
}
