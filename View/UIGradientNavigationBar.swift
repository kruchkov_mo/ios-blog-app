//
//  UIGradientNavigationBar.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/22/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

public class UIGradientNavigationBar: UINavigationBar {
    
    private var isAdded = false
    
    public private(set) weak var gradientLayer: CAGradientLayer?
    
    private func getGradLayer() -> CAGradientLayer? {
        if let sublayers = self.layer.sublayers {
            for subLayer in sublayers {
                if let subGradLayer = subLayer as? CAGradientLayer {
                    return subGradLayer
                }
            }
        }
        return nil
    }
    
    @objc public dynamic var colors: [UIColor]? {
        didSet {
            guard let colors = self.colors else {
                self.gradientLayer?.removeFromSuperlayer()
                return
            }
            layoutIfNeeded()
            
            let layer: CAGradientLayer
            if !self.isAdded {
                layer = CAGradientLayer()
                self.layer.insertSublayer(layer, at: 1)
                self.isAdded = true
               
            } else {
                layer = getGradLayer()!
            }
            
            layer.colors = colors.map { $0.cgColor }
            
            if let locations = self.locations {
                layer.locations = locations as [NSNumber]
            }
            
            layer.startPoint = startPoint
            layer.endPoint = endPoint
            
            self.gradientLayer = layer
        }
    }
    
    @objc public dynamic var locations: [Float]? {
        didSet {
            guard let layer = self.gradientLayer, let locations = self.locations else {
                self.gradientLayer?.locations = nil
                return
            }
            layer.locations = locations as [NSNumber]
        }
    }
    
    @objc public dynamic var startPoint = CGPoint(x: 0, y: 0) {
        didSet {
            if let layer = self.gradientLayer {
                layer.startPoint = self.startPoint
            }
        }
    }
    
    @objc public dynamic var endPoint = CGPoint(x: 0, y: 0) {
        didSet {
            if let layer = self.gradientLayer {
                layer.endPoint = self.endPoint
            }
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        guard let layer = self.gradientLayer else { return }
        layer.frame = CGRect(x: 0, y: 0 - statusBarHeight, width: self.bounds.width, height: statusBarHeight + self.bounds.height)
    }
}
