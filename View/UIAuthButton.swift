//
//  UIAuthButton.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/22/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

class UIAuthButton: UIButton {

    @objc dynamic var borderColor: UIColor? {
        get {
            if let cgColor = layer.borderColor {
                return UIColor(cgColor: cgColor)
            }
            return nil
        }
        set { layer.borderColor = newValue?.cgColor }
    }
    
    @objc dynamic var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
}
