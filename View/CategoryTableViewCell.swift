//
//  CategoryTableViewCell.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/10/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var catTitle: UILabel!
    @IBOutlet weak var catDescription: UILabel!
}
