# Blog application

### Blog mobile application for iOS

#### Blog with categories and articles. The application allows you to browse categories, choose article and like or write comment on it. Also it support authorization via Google

![Category screen](blog_iphone_6s.png)
