//
//  Post.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/10/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

struct Post {
    var postId: Int
    var userId: Int
    var title: String
    var body: String
}
