//
//  NetworkUtil.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/14/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation
import Alamofire

class NetworkUtil {
    static func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    static let FETCHING_TIMEOUT: UInt32 = 2000  // ms
}
