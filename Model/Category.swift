//
//  Category.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/10/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

struct Category {
    var categoryId: Int
    var description: String
    var name: String
}
