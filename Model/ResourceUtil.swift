//
//  ResourceUtil.swift
//  BlogMobileApp
//
//  Created by max kruchkov on 9/11/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

import Foundation

class ResourceUtil {
    
    static let FILE_NAME = "api_config"
    
    static func getApiUrl(key: String) -> String {
        if let path = Bundle.main.path(forResource: ResourceUtil.FILE_NAME, ofType: "plist"),
            let myDict = NSDictionary(contentsOfFile: path) {
            if let entityApi = myDict.value(forKey: key) as? String {
                return entityApi
            }
        }
        
        return ""
    }
}
